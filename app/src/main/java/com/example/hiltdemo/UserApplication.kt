package com.example.hiltdemo

import android.app.Application
import android.util.Log
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class UserApplication : Application() {


//    @Inject lateinit var userRepository: UserRepository // Field injection
    override fun onCreate() {
        super.onCreate()
        Log.d("","----------------UserApplication.onCreate() Executing-------------------------")
//        userRepository.saveUser("userApplication@gmail.com" , "UserApplication")

    }
}