package com.example.hiltdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import javax.inject.Named

@AndroidEntryPoint
class MainActivity : AppCompatActivity() { //Dependency Injection in activity

    @Inject
    @SQLQualifier
    lateinit var userRepository: UserRepository // Field injection
    override fun onCreate(savedInstanceState: Bundle?) {
    Log.d("","----------------MainActivity.onCreate() Executing - 1-------------------------")
        super.onCreate(savedInstanceState)
    Log.d("","----------------MainActivity.onCreate() Executing - 2-------------------------")
        setContentView(R.layout.activity_main)
    Log.d("","----------------MainActivity.onCreate() Executing - 3-------------------------")
        userRepository.saveUser("MainActivity@gmail.com" , "MainActivity")
    }
}