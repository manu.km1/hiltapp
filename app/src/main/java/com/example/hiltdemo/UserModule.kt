package com.example.hiltdemo

import android.util.Log
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import javax.inject.Named

@InstallIn(SingletonComponent::class)
@Module
class UserModule {
    @Provides
    @SQLQualifier
    fun providesSQLRepository(sqlRepository : SQLRepository) : UserRepository {
        Log.d("","----------------UserModule.providesSQLRepository() Executing-------------------------")
        return sqlRepository
    }
    @Provides
    @MongoDBQualifier
   fun providesMongoRepository() : UserRepository {
      Log.d("","----------------UserModule.providesMongoRepository() Executing-------------------------")
      return MongoDBReposityory()
   }

}