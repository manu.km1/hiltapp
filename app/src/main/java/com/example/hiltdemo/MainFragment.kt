package com.example.hiltdemo

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import javax.inject.Named

@AndroidEntryPoint
class MainFragment : Fragment() { //Dependency Injection in fragment
    @Inject
    @MongoDBQualifier
    lateinit var userRepository : UserRepository
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        Log.d("", "----------------MainFragment.onCreateView() Executing-------------------------")
        userRepository.saveUser("MainFragment@gmaul.com", "MainFragment")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }
}