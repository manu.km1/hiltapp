package com.example.hiltdemo

import android.util.Log
import javax.inject.Inject

const val TAG = "MANU"

interface UserRepository {
    fun saveUser(email : String , pass : String)
}


class SQLRepository @Inject constructor() :  UserRepository { // Contructor Injection
    override fun saveUser(email : String, pass : String){
        Log.d("","----------------UserRepository.SQLRepository.saveUser() Executing-------------------------")
        Log.d(TAG,"-------User Saved in SQL DB------")
    }
}

class MongoDBReposityory : UserRepository {
    override fun saveUser(email: String, pass: String) {
        Log.d("","----------------UserRepository.MongoDBReposityory.saveUser() Executing-------------------------")
        Log.d(TAG,"-------User Saved in MongoDB------")
    }

}